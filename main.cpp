#include <fstream>
#include "ListBidirectional.h"
#include "FileParser.h"

#define Less 0
#define More 1

int main(void)
{
	int countOfTests = 11;
	for (int iteration = 0; iteration < countOfTests; iteration++)
	{
		std::string inputName = "";
		std::string resultName = "";
		std::string resultString;
		switch (iteration)
		{
		case /* constant-expression */:
			/* code */
			break;

		default:
			std::ifstream input(inputName);
			std::ifstream resultStream(resultName);
			std::getline(resultStream, resultString, '\0');

			ListBidirectional listBidirectional;
			FileParser fileParser;
			fileParser.parseFromFileInto(input, listBidirectional);

			if (listBidirectional.to_string() == resultString)
			{
				std::cout << "Success!\n";
			}
			else
			{
				std::cout << "Error!\n";
			}
			break;
		}
	}

	std::string inputName = "tests/in_1.txt";
	std::string resultName = "tests/out_1.txt";
	std::string resultString;

	std::ifstream input(inputName);
	std::ifstream resultStream(resultName);
	std::getline(resultStream, resultString, '\0');

	ListBidirectional listBidirectional;
	FileParser fileParser;
	fileParser.parseFromFileInto(input, listBidirectional);
	if (listBidirectional.to_string() == resultString)
	{
		std::cout << "Success!\n";
	}
	else
	{
		std::cout << "Error!\n";
	}

	input.close();
	resultStream.close();

	return 0;
}