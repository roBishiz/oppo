#include <iostream>
#include <algorithm>
#include <string>
#include <fstream>
#include <cstring>
#include <vector>
#include <time.h>
#include <stdlib.h>
#include <regex>



std::string Functional = "Functional";
std::string ObjectOriented = "ObjectOriented";
std::string Procedural = "Procedural";
std::string InheritanceSingle = "Inheritance:Single"; //Наследование:Одиночное
std::string InheritanceMultiple = "Inheritance:Multiple"; //Наследование:Множественное
std::string Applicative = "Applicative";
std::string Combinatorial = "Combinatorial";
std::string ATDYes = "ATD:Yes";
std::string ATDNo = "ATD:No";

class ProgLang{
public:
	std::string name;
	std::string typeProgLang, atr;
	int year;
	virtual std::string to_string(void);
};

std::string ProgLang::to_string(void){
	return name + ' ' + typeProgLang + ' ' + atr + ' ' + std::to_string(year);
}

class FunctionalProgLang : public ProgLang{
public:
	FunctionalProgLang(std::string _name, std::string _typeProgLang, std::string _atr, int _year);
};

FunctionalProgLang::FunctionalProgLang(std::string _name, std::string _typeProgLang, std::string _atr, int _year){
	name = _name;
	typeProgLang = _typeProgLang;
    atr = _atr;
    year = _year;
}

class ObjectOrientedProgLang : public ProgLang{
public:
	ObjectOrientedProgLang(std::string _name, std::string _typeProgLang, std::string _atr, int _year);
};

ObjectOrientedProgLang::ObjectOrientedProgLang(std::string _name, std::string _typeProgLang, std::string _atr, int _year){
	name = _name;
	typeProgLang = _typeProgLang;
    atr = _atr;
    year = _year;
}

class ProceduralProgLang : public ProgLang{
public:
	ProceduralProgLang(std::string _name, std::string _typeProgLang, std::string _atr, int _year);
};

ProceduralProgLang::ProceduralProgLang(std::string _name, std::string _typeProgLang, std::string _atr, int _year){
	name = _name;
	typeProgLang = _typeProgLang;
    atr = _atr;
    year = _year;
}

struct ProgLangCheck{
	std::string progLang;
	std::vector<std::string> atrs;
};

struct Node
{
	Node *nextNode;
	Node *prevNode;
	ProgLang *progLang;
};

class ListBidirectional{
private:
	void deleteNode(Node*);
public:
	ListBidirectional(){};
	void push_back(ProgLang*);
	void deleteThen(int, int);
	void deleteThen(std::string);
	void printInFile(std::ofstream&);
	void sort(void);
	Node *curentNode;
	Node *startNode;
	unsigned int _size = 0;
};

void ListBidirectional::deleteThen(int side, int year){
	Node *tempNode = startNode;
	while(tempNode != NULL){
		if (side){
			if (tempNode->progLang->year > year){
				deleteNode(tempNode);
			}  
		} else{
			if (tempNode->progLang->year < year){
				deleteNode(tempNode);
			}
		}
		tempNode = tempNode->nextNode;
	}
}

void ListBidirectional::deleteThen(std::string typeProgLang){
	int iteration = 0;
	Node *tempNode = startNode;
	while(tempNode != NULL){
		if (tempNode->progLang->typeProgLang == typeProgLang){
			deleteNode(tempNode);
		}
		tempNode = tempNode->nextNode;
		iteration++;
	}
}

void ListBidirectional::deleteNode(Node *node){
	Node *firstTempNode = node->nextNode;
	if (firstTempNode != NULL){
		if (node == startNode)
			startNode = firstTempNode;
		firstTempNode->prevNode = node->prevNode;
	}
	firstTempNode = node->prevNode;
	if (firstTempNode != NULL){
		firstTempNode->nextNode = node->nextNode;
	}
}

void ListBidirectional::push_back(ProgLang *progLang){
	if(_size){
		Node *tempNode = new Node();
		tempNode->progLang = progLang;
		tempNode->nextNode = NULL;
		tempNode->prevNode = curentNode;
		curentNode->nextNode = tempNode;
		curentNode = tempNode;
	} else {
		curentNode = new Node();
		curentNode->progLang = progLang;
		startNode = curentNode;
	}
	_size++;
}

void ListBidirectional::sort(void){
	for(int i = 1; i < _size; i++){
		Node *firstTempNode = startNode;
		Node *secondTempNode = firstTempNode->nextNode;
		while (secondTempNode != NULL){
			if (firstTempNode->progLang->year > secondTempNode->progLang->year){
				secondTempNode->prevNode = firstTempNode->prevNode;
				firstTempNode->nextNode = secondTempNode->nextNode;
				firstTempNode->prevNode = secondTempNode;
				secondTempNode->nextNode = firstTempNode;
				Node *thirdTempNode = firstTempNode->nextNode;
				if (thirdTempNode != NULL){
					thirdTempNode->prevNode = firstTempNode;
				}
				thirdTempNode = secondTempNode->prevNode;
				if (thirdTempNode != NULL){
					thirdTempNode->nextNode = secondTempNode;
				}
				secondTempNode = firstTempNode->nextNode;
			} else {
				firstTempNode = secondTempNode;
				secondTempNode = firstTempNode->nextNode;
			}
		}
	}
}

void ListBidirectional::printInFile(std::ofstream &file){
	if (_size){
		int iteration = 0;
		std::string outputString = "";
		Node *tempNode = startNode;
		while(tempNode != NULL){
			outputString += "{" + std::to_string(iteration) + "} : " + tempNode->progLang->to_string() + '\n';
			tempNode = tempNode->nextNode;
			iteration++;
		}
		file << outputString;
	}
	else{
		file << "Empty\n";
	}
}

class FileParser{
public:
	FileParser(){};
	void parseFromFileInto(std::ifstream&, ListBidirectional&);
};

void FileParser::parseFromFileInto(std::ifstream &inputFile, ListBidirectional &listBidirectional){
	std::string str;

	std::vector<ProgLangCheck> vectorProgLangCheck;
	ProgLangCheck progLangCheck;
	progLangCheck.progLang = Procedural;
	progLangCheck.atrs = { ATDYes, ATDNo };
	vectorProgLangCheck.push_back(progLangCheck);
	progLangCheck.progLang = ObjectOriented;
	progLangCheck.atrs = { InheritanceSingle, InheritanceMultiple };
	vectorProgLangCheck.push_back(progLangCheck);
	progLangCheck.progLang = Functional;
	progLangCheck.atrs = { Applicative, Combinatorial };

	while (getline(inputFile, str))
	{
		std::vector<std::string> inputAtr;
		int countAtr = 0;
		int findSpace = str.find(" ");
		while (findSpace != -1)
		{
			countAtr++;
			inputAtr.push_back(str.substr(0, findSpace));
			str = str.substr(findSpace + 1, str.length());
			findSpace = str.find(" ");
		}
		inputAtr.push_back(str.substr(0, str.length()));

		if (inputAtr.size() != 4)
		{
			std::cout << "Error!";
			continue;
		}

		bool flagCheck = true;
		for (auto &ProgLangCheck : vectorProgLangCheck)
		{
			if (ProgLangCheck.progLang == inputAtr[1])
			{
				std::vector<std::string>::iterator it = find(ProgLangCheck.atrs.begin(), ProgLangCheck.atrs.end(), inputAtr[2]);
				if (it != ProgLangCheck.atrs.end())
				{
					std::regex rex{ "(\\d\\d\\d)\\d" };
					std::smatch year_match;
					if (std::regex_match(inputAtr[3], year_match, rex)){
						int yearPart = atoi(year_match[1].str().c_str());
						if (yearPart > 198 && yearPart < 201){
							int year = atoi(year_match[0].str().c_str());
							if (ProgLangCheck.progLang == Procedural){
								ProgLang *proceduralProgLang = new ProceduralProgLang(inputAtr[0], inputAtr[1], inputAtr[2], year);
								listBidirectional.push_back(proceduralProgLang);
								//ProgLang *progLang = new ProceduralProgLang(inputAtr[0], inputAtr[1], year);
							}
							else if (ProgLangCheck.progLang == ObjectOriented){
								ProgLang *objectOrientedProgLang = new ObjectOrientedProgLang(inputAtr[0], inputAtr[1], inputAtr[2], year);
								listBidirectional.push_back(objectOrientedProgLang);
								//ProgLang *progLang = new ObjectOrientedProgLang(inputAtr[0], inputAtr[1], year);
							}
							else if (ProgLangCheck.progLang == Functional){
								ProgLang *functionalProgLang = new FunctionalProgLang(inputAtr[0], inputAtr[1], inputAtr[2], year);
								listBidirectional.push_back(functionalProgLang);
								//ProgLang *progLang = new FunctionalProgLang(inputAtr[0], inputAtr[1], year);
							}
							break;
						}
					}
				}
			}
		}
	}
}

int main(void){
	
	std::ofstream output("output");
	std::ifstream input("input");

	ListBidirectional listBidirectional;

	FileParser fileParser;
	fileParser.parseFromFileInto(input, listBidirectional);
	listBidirectional.printInFile(output);
	
	output.close();
	input.close();

	return 0;
}