#include "ObjectOrientedProgLang.h"
#include <string>

ObjectOrientedProgLang::ObjectOrientedProgLang(std::string _name, std::string _typeProgLang, std::string _atr, int _year){
	name = _name;
	typeProgLang = _typeProgLang;
    atr = _atr;
    year = _year;
}