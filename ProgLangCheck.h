#ifndef PROGLANGCHECK_H
#define PROGLANGCHECK_H
#include <string>
#include <vector>

struct ProgLangCheck{
	std::string progLang;
	std::vector<std::string> atrs;
};

#endif