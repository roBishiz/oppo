#ifndef FILEPARSER_H
#define FILEPARSER_H
#include <fstream>
#include "ListBidirectional.h"

class FileParser{
public:
	FileParser(){};
	void parseFromFileInto(std::ifstream&, ListBidirectional&);
};

#endif