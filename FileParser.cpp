#include "FileParser.h"
#include <fstream>
#include "ListBidirectional.h"
#include <string>
#include <regex>
#include "ProgLangCheck.h"
#include "ObjectOrientedProgLang.h"
#include "FunctionalProgLang.h"
#include "ProceduralProgLang.h"

std::string Functional = "Functional";
std::string ObjectOriented = "ObjectOriented";
std::string Procedural = "Procedural";
std::string InheritanceSingle = "Inheritance:Single"; //Наследование:Одиночное
std::string InheritanceMultiple = "Inheritance:Multiple"; //Наследование:Множественное
std::string Applicative = "Applicative";
std::string Combinatorial = "Combinatorial";
std::string ATDYes = "ATD:Yes";
std::string ATDNo = "ATD:No";

void FileParser::parseFromFileInto(std::ifstream &inputFile, ListBidirectional &listBidirectional){
	std::string str;

	std::vector<ProgLangCheck> vectorProgLangCheck;
	ProgLangCheck progLangCheck;
	progLangCheck.progLang = Procedural;
	progLangCheck.atrs = { ATDYes, ATDNo };
	vectorProgLangCheck.push_back(progLangCheck);
	progLangCheck.progLang = ObjectOriented;
	progLangCheck.atrs = { InheritanceSingle, InheritanceMultiple };
	vectorProgLangCheck.push_back(progLangCheck);
	progLangCheck.progLang = Functional;
	progLangCheck.atrs = { Applicative, Combinatorial };
	vectorProgLangCheck.push_back(progLangCheck);

	while (getline(inputFile, str))
	{
		std::vector<std::string> inputAtr;
		int countAtr = 0;
		int findSpace = str.find(" ");
		while (findSpace != -1)
		{
			countAtr++;
			inputAtr.push_back(str.substr(0, findSpace));
			str = str.substr(findSpace + 1, str.length());
			findSpace = str.find(" ");
		}
		inputAtr.push_back(str.substr(0, str.length()));

		if (inputAtr.size() != 4)
		{
			std::cout << "Error!";
			continue;
		}

		bool flagCheck = true;
		for (auto &ProgLangCheck : vectorProgLangCheck)
		{
			if (ProgLangCheck.progLang == inputAtr[1])
			{
				std::vector<std::string>::iterator it = find(ProgLangCheck.atrs.begin(), ProgLangCheck.atrs.end(), inputAtr[2]);
				if (it != ProgLangCheck.atrs.end())
				{
					std::regex rex{ "(\\d\\d\\d)\\d" };
					std::smatch year_match;
					if (std::regex_match(inputAtr[3], year_match, rex)){
						int yearPart = atoi(year_match[1].str().c_str());
						if (yearPart > 198 && yearPart < 201){
							int year = atoi(year_match[0].str().c_str());
							if (ProgLangCheck.progLang == Procedural){
								ProgLang *proceduralProgLang = new ProceduralProgLang(inputAtr[0], inputAtr[1], inputAtr[2], year);
								listBidirectional.push_back(proceduralProgLang);
								//ProgLang *progLang = new ProceduralProgLang(inputAtr[0], inputAtr[1], year);
							}
							else if (ProgLangCheck.progLang == ObjectOriented){
								ProgLang *objectOrientedProgLang = new ObjectOrientedProgLang(inputAtr[0], inputAtr[1], inputAtr[2], year);
								listBidirectional.push_back(objectOrientedProgLang);
								//ProgLang *progLang = new ObjectOrientedProgLang(inputAtr[0], inputAtr[1], year);
							}
							else if (ProgLangCheck.progLang == Functional){
								ProgLang *functionalProgLang = new FunctionalProgLang(inputAtr[0], inputAtr[1], inputAtr[2], year);
								listBidirectional.push_back(functionalProgLang);
								//ProgLang *progLang = new FunctionalProgLang(inputAtr[0], inputAtr[1], year);
							}
							break;
						}
					}
				}
			}
		}
	}
}