#ifndef LISTBIDIRECTIONAL_H
#define LISTBIDIRECTIONAL_H
#include "Node.h"
#include "ProgLang.h"
#include <iostream>

class ListBidirectional{
private:
	void deleteNode(Node*);
public:
	ListBidirectional(){};
	void push_back(ProgLang*);
	void deleteThen(int, int);
	void deleteThen(std::string);
	std::string to_string();
	void sort(void);
	Node *curentNode;
	Node *startNode;
	unsigned int _size = 0;
};

#endif