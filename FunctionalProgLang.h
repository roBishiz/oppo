#ifndef FunctionalProgLang_H
#define FunctionalProgLang_H
#include "ProgLang.h"
#include <string>

class FunctionalProgLang : public ProgLang{
public:
	FunctionalProgLang(std::string _name, std::string _typeProgLang, std::string _atr, int _year);
};

#endif