#ifndef NODE_H
#define NODE_H
#include "ProgLang.h"

struct Node
{
	Node *nextNode;
	Node *prevNode;
	ProgLang *progLang;
};

#endif