#ifndef ObjectOrientedProgLang_H
#define ObjectOrientedProgLang_H
#include "ProgLang.h"
#include <string>

class ObjectOrientedProgLang : public ProgLang{
public:
	ObjectOrientedProgLang(std::string _name, std::string _typeProgLang, std::string _atr, int _year);
};

#endif