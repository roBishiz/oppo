#ifndef ProceduralProgLang_H
#define ProceduralProgLang_H
#include "ProgLang.h"
#include <string>

class ProceduralProgLang : public ProgLang{
public:
	ProceduralProgLang(std::string _name, std::string _typeProgLang, std::string _atr, int _year);
};

#endif