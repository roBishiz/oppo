#ifndef PROGLANG_H
#define PROGLANG_H
#include <string>

class ProgLang{
public:
	std::string name;
	std::string typeProgLang, atr;
	int year;
	virtual std::string to_string(void);
};

#endif