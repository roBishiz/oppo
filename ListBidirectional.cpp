#include "ListBidirectional.h"

void ListBidirectional::deleteThen(int side, int year){
	Node *tempNode = startNode;
	while(tempNode != NULL){
		if (side){
			if (tempNode->progLang->year > year){
				deleteNode(tempNode);
			}  
		} else{
			if (tempNode->progLang->year < year){
				deleteNode(tempNode);
			}
		}
		tempNode = tempNode->nextNode;
	}
}

void ListBidirectional::deleteThen(std::string typeProgLang){
	int iteration = 0;
	Node *tempNode = startNode;
	while(tempNode != NULL){
		if (tempNode->progLang->typeProgLang == typeProgLang){
			deleteNode(tempNode);
		}
		tempNode = tempNode->nextNode;
		iteration++;
	}
}

void ListBidirectional::deleteNode(Node *node){
	Node *firstTempNode = node->nextNode;
	if (firstTempNode != NULL){
		if (node == startNode)
			startNode = firstTempNode;
		firstTempNode->prevNode = node->prevNode;
	}
	firstTempNode = node->prevNode;
	if (firstTempNode != NULL){
		firstTempNode->nextNode = node->nextNode;
	}
}

void ListBidirectional::push_back(ProgLang *progLang){
	if(_size){
		Node *tempNode = new Node();
		tempNode->progLang = progLang;
		tempNode->nextNode = NULL;
		tempNode->prevNode = curentNode;
		curentNode->nextNode = tempNode;
		curentNode = tempNode;
	} else {
		curentNode = new Node();
		curentNode->progLang = progLang;
		startNode = curentNode;
	}
	_size++;
}

void ListBidirectional::sort(void){
	for(int i = 1; i < _size; i++){
		Node *firstTempNode = startNode;
		Node *secondTempNode = firstTempNode->nextNode;
		while (secondTempNode != NULL){
			if (firstTempNode->progLang->year > secondTempNode->progLang->year){
				secondTempNode->prevNode = firstTempNode->prevNode;
				firstTempNode->nextNode = secondTempNode->nextNode;
				firstTempNode->prevNode = secondTempNode;
				secondTempNode->nextNode = firstTempNode;
				Node *thirdTempNode = firstTempNode->nextNode;
				if (thirdTempNode != NULL){
					thirdTempNode->prevNode = firstTempNode;
				}
				thirdTempNode = secondTempNode->prevNode;
				if (thirdTempNode != NULL){
					thirdTempNode->nextNode = secondTempNode;
				}
				secondTempNode = firstTempNode->nextNode;
			} else {
				firstTempNode = secondTempNode;
				secondTempNode = firstTempNode->nextNode;
			}
		}
	}
}

std::string ListBidirectional::to_string(){
	if (_size){
		int iteration = 0;
		std::string outputString = "";
		Node *tempNode = startNode;
		while(tempNode != NULL){
			outputString += "{" + std::to_string(iteration) + "} : " + tempNode->progLang->to_string() + '\n';
			tempNode = tempNode->nextNode;
			iteration++;
		}
		return outputString;
	}
	else{
		return "Empty";
	}
}